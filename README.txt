syslog for Pd
=============

allows to send syslog messages of a settable severity from with Pd.


installation
-----------
run:
$ make

finally, install it with:
$ make install
this will install everything needed to /usr/local/lib/pd-externals/syslog;
you can change the default installation target via the `pkblibdir` variable:
$ make install pkglibdir=/usr/lib/pd/extra

running
-------
create a [syslog] object and use it like [print]
