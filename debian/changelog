pd-syslog (0.1-7) unstable; urgency=medium

  * Build for both single and double-precision Pd
  * Switch build-system to pd-lib-builder
    + Replace upstream build-system with pd-lib-builder
  * Update d/copyright dates
  * Bump standards version to 4.6.2

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 06 Jul 2023 10:35:55 +0200

pd-syslog (0.1-6) unstable; urgency=medium

  * Modernize 'licensecheck' target
    + Ensure that 'licensecheck' is run with the C.UTF-8 locale
    + Exclude debian/ from 'licensecheck'
    + Re-generate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 07 Dec 2022 10:52:40 +0100

pd-syslog (0.1-5) unstable; urgency=medium

  * Modernize 'licensecheck' target
    + Re-generate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 05 Dec 2022 11:13:56 +0100

pd-syslog (0.1-4) unstable; urgency=medium

  * Apply "warp-and-sort -ast"
  * Bump dh-compat to 13
  * Bump standards version to 4.6.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sun, 04 Sep 2022 16:53:06 +0200

pd-syslog (0.1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Fix FTCBFS: Defer stripping to dh_strip.
    Thanks to Helmut Grohne <helmut@subdivi.de> (Closes: #958354)
  * Add salsa-ci configuration
  * Remove obsolete file d/source/local-options
  * Declare that building this package does not require 'root' powers.
  * Bump dh-compat to 12
  * Bump standards version to 4.5.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 21 Apr 2020 15:39:51 +0200

pd-syslog (0.1-2) unstable; urgency=medium

  * Switched buildsystem from dh to cdbs
    * Bumped dh compat to 11
    * Enabled hardening
  * Switched URLs to https://
  * Updated Vcs-* stanzas to salsa.d.o
  * Updated maintainer address
  * Updated d/copyright_hints
  * Bumped standards version to 4.1.3

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 01 Feb 2018 23:40:04 +0100

pd-syslog (0.1-1) unstable; urgency=low

  * Initial release. (Closes: #603177)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 17 Aug 2015 16:07:49 +0200
